﻿using CommandLine;
using NiceUnitTesting.Running;
using System.Threading.Tasks;

namespace NiceUnitTesting.Runner
{
  internal static class Program
  {
    private static async Task Main(string[] args)
    {
      await Parser.Default.ParseArguments<RunnerConfiguration>(args)
        .WithParsedAsync(NiceUnitTestingRunner.Run).ConfigureAwait(false);
    }
  }
}
