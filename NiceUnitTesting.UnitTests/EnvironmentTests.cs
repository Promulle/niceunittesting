﻿using FluentAssertions;
using NiceUnitTesting.UnitTests.Environments;
using System;

namespace NiceUnitTesting.UnitTests
{
  /// <summary>
  /// Tests for environment
  /// </summary>
  [TestClass]
  [Environment(ImplementationType = typeof(TestingEnvironment))]
  [Environment(ImplementationType = typeof(SecondTestEnvironment))]
  public class EnvironmentTests : IToBeTested
  {
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Test
    /// </summary>
    [Test]
    public void ValueSet()
    {
      Name.Should().Be(TestingEnvironment.VALUE);
    }
  }
}
