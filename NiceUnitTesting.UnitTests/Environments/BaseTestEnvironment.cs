﻿using NiceUnitTesting.TestEnvironment;
using NiceUnitTesting.Tests;

namespace NiceUnitTesting.UnitTests.Environments
{
  /// <summary>
  /// BaseEnvironment for testing
  /// </summary>
  public abstract class BaseTestEnvironment : ITestEnvironment
  {
    /// <summary>
    /// Name
    /// </summary>
    public abstract string Name { get; }

    /// <summary>
    /// Enables threading
    /// </summary>
    public abstract bool EnableThreading { get; }

    /// <summary>
    /// Cache: yes
    /// </summary>
    public bool IsCacheable { get; } = true;

    /// <summary>
    /// Prepare
    /// </summary>
    /// <param name="i_Fixture"></param>
    public abstract void Prepare(object i_Fixture);

    /// <summary>
    /// Run complete
    /// </summary>
    public virtual void RunComplete()
    {
    }

    /// <summary>
    /// OnTestDone
    /// </summary>
    /// <param name="i_Sender"></param>
    /// <param name="i_Test"></param>
    public virtual void OnTestDone(object i_Sender, TestTask i_Test)
    {
    }

    /// <summary>
    /// OnTaskDone
    /// </summary>
    /// <param name="i_Sender"></param>
    /// <param name="i_Task"></param>
    public virtual void OnTaskDone(object i_Sender, TestTask i_Task)
    {
    }
  }
}
