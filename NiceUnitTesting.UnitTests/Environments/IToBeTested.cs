﻿namespace NiceUnitTesting.UnitTests.Environments
{
  /// <summary>
  /// Inteface for testing of manipulation of testfixtures
  /// </summary>
  public interface IToBeTested
  {
    /// <summary>
    /// Name
    /// </summary>
    string Name { get; set; }
  }
}
