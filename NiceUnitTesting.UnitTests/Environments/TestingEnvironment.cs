﻿namespace NiceUnitTesting.UnitTests.Environments
{
  /// <summary>
  /// TestEnv impl
  /// </summary>
  public class TestingEnvironment : BaseTestEnvironment
  {
    /// <summary>
    /// Value
    /// </summary>
    public static readonly string VALUE = "Value";
    /// <summary>
    /// Name
    /// </summary>
    public override string Name
    {
      get { return GetType().Name; }
    }
    /// <summary>
    /// Threading
    /// </summary>
    public override bool EnableThreading
    {
      get { return false; }
    }
    /// <summary>
    /// Prepare
    /// </summary>
    /// <param name="i_Fixture"></param>
    public override void Prepare(object i_Fixture)
    {
      if (i_Fixture is IToBeTested casted)
      {
        casted.Name = VALUE;
      }
    }
  }
}
