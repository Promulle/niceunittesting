﻿using FluentAssertions;

namespace NiceUnitTesting.UnitTests
{
  /// <summary>
  /// Tests for priority
  /// </summary>
  [TestClass]
  public class PriorityTests
  {
    private bool m_SetupOneHasRun;
    private bool m_SetupTwoHasRun;
    private bool m_AllSetupsHaveRun;

    /// <summary>
    /// First setup
    /// </summary>
    [Setup(Priority = 0)]
    public void SetupOne()
    {
      m_SetupOneHasRun = true;
    }

    /// <summary>
    /// Second setup
    /// </summary>
    [Setup(Priority = 1)]
    public void SetupTwo()
    {
      m_SetupOneHasRun.Should().BeTrue();
      m_SetupTwoHasRun = true;
    }

    /// <summary>
    /// Third setup
    /// </summary>
    [Setup(Priority = 2)]
    public void SetupThree()
    {
      m_SetupOneHasRun.Should().BeTrue();
      m_SetupTwoHasRun.Should().BeTrue();
      m_AllSetupsHaveRun = true;
    }

    /// <summary>
    /// Test
    /// </summary>
    [Test]
    public void TaskPriority()
    {
      m_SetupOneHasRun.Should().BeTrue();
      m_SetupTwoHasRun.Should().BeTrue();
      m_AllSetupsHaveRun.Should().BeTrue();
    }
  }
}
