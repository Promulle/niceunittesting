﻿using NiceUnitTesting.Running;
using System.Threading.Tasks;

namespace NiceUnitTesting.UnitTests
{
  internal static class Program
  {
    private static async Task Main()
    {
      var config = RunnerConfiguration.New()
        .AddThisAssembly()
        .ShouldWait(true)
        .ShowSearchErrors(true)
        .UseThreads(true)
        .CleanLogDirectory(true)
        .DisplayStatistic(true)
        .Create();

      await NiceUnitTestingRunner.Run(config).ConfigureAwait(false);
    }
  }
}
