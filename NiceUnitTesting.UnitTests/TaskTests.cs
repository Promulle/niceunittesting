﻿using FluentAssertions;
using System;
using System.Threading.Tasks;

namespace NiceUnitTesting.UnitTests
{
  /// <summary>
  /// Test for task returning method
  /// </summary>
  [TestClass]
  public class TaskTests
  {
    private bool m_Flag = false;

    /// <summary>
    /// Tests async execution
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task AsyncFunc()
    {
      await Task.Run(() => m_Flag = true).ConfigureAwait(false);
      Console.WriteLine("Run!");
      m_Flag.Should().BeTrue();
    }
  }
}
