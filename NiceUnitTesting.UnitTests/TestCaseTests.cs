﻿using FluentAssertions;

namespace NiceUnitTesting.UnitTests
{
  /// <summary>
  /// Fixture for test case testing
  /// </summary>
  [TestClass]
  public class TestCaseTests
  {
    private int m_Setups;
    private int m_Adds;

    /// <summary>
    /// Setup
    /// </summary>
    [Setup]
    public void MySetup()
    {
      m_Setups++;
    }

    /// <summary>
    /// Adds two numbers
    /// </summary>
    /// <param name="i_Left"></param>
    /// <param name="i_Right"></param>
    [Test]
    [TestCase(5, 10)]
    [TestCase(10, 20)]
    public void Add(int i_Left, int i_Right)
    {
      m_Adds++;
    }

    /// <summary>
    /// Onetime tear down
    /// </summary>
    public void OneTimeTear()
    {
      m_Adds.Should().Be(m_Setups);
    }
  }
}
