﻿using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute for Name of Test-Author
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
  public class AuthorAttribute : Attribute
  {
    /// <summary>
    /// Constructor defines required parameter name
    /// </summary>
    /// <param name="name"></param>
    public AuthorAttribute(string name)
    {
      Name = name;
    }

    /// <summary>
    /// Name of Test Author
    /// </summary>
    public virtual string Name { get; protected set; }
  }
}
