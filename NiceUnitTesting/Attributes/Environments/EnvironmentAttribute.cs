﻿using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute defining environment impl
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
  public class EnvironmentAttribute : Attribute
  {
    /// <summary>
    /// Environment to run tests in. This type has to implement ITestEnvironment, otherwise it will not be used
    /// </summary>
    public Type ImplementationType { get; set; }
  }
}
