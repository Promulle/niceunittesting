﻿using NiceUnitTesting.Attributes.Infrastructure;
using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute marking method for One-Time-Setup
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public class OneTimeSetupAttribute : PrioritizableTaskAttribute
  {
  }
}
