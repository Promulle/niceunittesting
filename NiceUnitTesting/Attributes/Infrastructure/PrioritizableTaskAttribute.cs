﻿using System;

namespace NiceUnitTesting.Attributes.Infrastructure
{

  /// <summary>
  /// Attribute that holds priority values
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public abstract class PrioritizableTaskAttribute : Attribute
  {
    /// <summary>
    /// Priority of this task
    /// </summary>
    public int Priority { get; set; }
  }
}
