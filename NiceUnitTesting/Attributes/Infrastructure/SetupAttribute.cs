﻿using NiceUnitTesting.Attributes.Infrastructure;
using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute marking method for setup
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public class SetupAttribute : PrioritizableTaskAttribute
  {
  }
}
