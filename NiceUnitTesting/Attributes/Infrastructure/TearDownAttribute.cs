﻿using NiceUnitTesting.Attributes.Infrastructure;
using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute marking method for TearDown
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public class TearDownAttribute : PrioritizableTaskAttribute
  {
  }
}
