﻿using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attribute marking method for test
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public class TestAttribute : Attribute
  {
    /// <summary>
    /// Whether the case should be using setups or not.
    /// Default: true
    /// </summary>
    public bool UseSetups { get; set; }

    /// <summary>
    /// Whether the case should be using teardowns or not.
    /// Default: true
    /// </summary>
    public bool UseTearDowns { get; set; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public TestAttribute()
    {
      UseSetups = true;
      UseTearDowns = true;
    }
  }
}

