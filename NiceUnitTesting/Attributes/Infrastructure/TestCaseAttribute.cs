﻿using System;
using System.Collections.Generic;

namespace NiceUnitTesting
{
  /// <summary>
  /// Attrbute for test cases
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
  public class TestCaseAttribute : Attribute
  {
    /// <summary>
    /// Parameters
    /// </summary>
    public List<object> Parameters { get; protected set; }

    /// <summary>
    /// Optional
    /// Name of TestCase
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Whether the case should be using setups or not.
    /// Default: true
    /// </summary>
    public bool UseSetups { get; set; }

    /// <summary>
    /// Whether the case should be using teardowns or not.
    /// Default: true
    /// </summary>
    public bool UseTearDowns { get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    public TestCaseAttribute(params object[] i_Parameters)
    {
      Parameters = new List<object>(i_Parameters);
      UseSetups = true;
      UseTearDowns = true;
    }
  }
}
