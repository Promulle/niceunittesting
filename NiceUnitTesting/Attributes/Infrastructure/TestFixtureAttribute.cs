﻿using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Marking method for test fixture
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
  public class TestClassAttribute : Attribute
  {
    /// <summary>
    /// Name of Test Author
    /// </summary>
    public virtual string Name { get; protected set; }
  }

}
