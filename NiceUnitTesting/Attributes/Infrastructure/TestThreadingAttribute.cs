﻿using System;

namespace NiceUnitTesting
{
  /// <summary>
  /// Atttribute that indicates how the test should be threaded
  /// </summary>
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
  public class TestThreadingAttribute : Attribute
  {
    /// <summary>
    /// Mode to run tests in
    /// </summary>
    public ThreadModes ThreadMode { get; set; }
  }
}
