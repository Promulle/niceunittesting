﻿using System;
using System.Collections.Generic;

namespace NiceUnitTesting.Base
{
  /// <summary>
  /// A cache
  /// </summary>
  internal class DefaultCache<TKey, TValue>
  {
    private readonly Dictionary<TKey, TValue> m_Cache = new Dictionary<TKey, TValue>();
    private readonly Func<TKey, TValue> m_FactoryMethod;

    /// <summary>
    /// Constructor that sets the to use factory method
    /// </summary>
    /// <param name="i_FactoryMethod"></param>
    public DefaultCache(Func<TKey, TValue> i_FactoryMethod)
    {
      m_FactoryMethod = i_FactoryMethod;
    }

    /// <summary>
    /// Returns a cached instance or creates a new one
    /// </summary>
    /// <param name="i_Key"></param>
    /// <returns></returns>
    public TValue GetOrCreate(TKey i_Key)
    {
      if (!m_Cache.TryGetValue(i_Key, out var value))
      {
        value = m_FactoryMethod(i_Key);
        m_Cache.Add(i_Key, value);
      }
      return value;
    }
  }
}
