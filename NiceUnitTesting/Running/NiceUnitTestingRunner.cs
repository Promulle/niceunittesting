﻿using NiceUnitTesting.Tests;
using NiceUnitTesting.Tests.Execution;
using NiceUnitTesting.Tests.Execution.Config;
using NiceUnitTesting.Tests.Execution.Fixtures;
using NiceUnitTesting.Tests.Search;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NiceUnitTesting.Running
{
  /// <summary>
  /// Class preparing and running tests
  /// </summary>
  public static class NiceUnitTestingRunner
  {
    private const string PARAM_COUNT_TESTS = "@countTests";
    private const string PARAM_ASSEMBLY = "@assembly";
    /// <summary>
    /// Run the configuration
    /// </summary>
    /// <param name="i_Config"></param>
    public static async Task Run(ITestingConfiguration i_Config)
    {
      var components = TestExecutionComponents.GetComponents(i_Config);
      Output(ProgramConstants.MESSAGE_START, components);
      Output(FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).FileVersion, components);
      var fixtureDefinitions = SpecifiedAssemblies.GetTestAssemblies(i_Config)
        .SelectMany(r => TestDiscovery.SearchAssemblyForTests(r, i_Config.HideSearchErrors));
      var executor = new TestRunFixtureExecutor();
      await executor.RunFixtures(fixtureDefinitions, i_Config.Threaded);
      var allResults = executor.ExecutedFixtures.SelectMany(r => r.Runner.Tasks.Tests)
        .Select(r => r.Result);
      components.Output.OutputResults(allResults);
      DisplayStatistic(allResults, components);
      Console.ReadLine();
    }

    private static void DisplayStatistic(IEnumerable<TestTaskResult> i_Results, TestExecutionComponents t_Components)
    {
      if (t_Components.Statistic != null)
      {
        var stat = t_Components.Statistic;
        stat.AddToStatistic(i_Results);
        Console.ForegroundColor = ConsoleColor.Cyan;
        stat.ToDisplayLines().ToList().ForEach(r => Output(r, t_Components));
        Console.ResetColor();
      }
    }

    private static void Output(string i_Text, TestExecutionComponents i_Components)
    {
      i_Components.Output.ConsoleOutput?.Output(i_Text);
    }
  }
}
