﻿namespace NiceUnitTesting.Running
{
  /// <summary>
  /// Class having constants for program.cs
  /// </summary>
  public static class ProgramConstants
  {
    /// <summary>
    /// Pattern for dll files
    /// </summary>
    public const string PATTERN_DLL = "*.dll";

    /// <summary>
    /// Message if no test dll is specified
    /// </summary>
    public const string MESSAGE_NO_SPECIFIED = "No assemblies to test specified. Searching every dll in directory:";

    /// <summary>
    /// Start Message
    /// </summary>
    public const string MESSAGE_START = "NiceUnit Testing Runner.";

    /// <summary>
    /// Option parameter
    /// </summary>
    public const string MARKER_OPTION = "-";
  }
}
