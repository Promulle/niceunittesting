﻿using CommandLine;
using NiceUnitTesting.Tests.Execution.Config;
using System.Collections.Generic;

namespace NiceUnitTesting.Running
{
  /// <summary>
  /// Config for Running unit tests
  /// </summary>
  public class RunnerConfiguration : ITestingConfiguration
  {
    /// <summary>
    /// Wait
    /// </summary>
    [Option('w', "wait", HelpText = "Wait for keypress after execution of tests.")]
    public bool Wait { get; set; }

    /// <summary>
    /// Threaded
    /// </summary>
    [Option('t', "threaded", HelpText = "Run tests/fixtures in separate threads.")]
    public bool Threaded { get; set; }

    /// <summary>
    /// Statistics
    /// </summary>
    [Option('s', "statistic", HelpText = "Keep a statistic of all tests run.")]
    public bool Statistic { get; set; }

    /// <summary>
    /// Cleanoutput dir
    /// </summary>
    [Option('c', "clean_outdir", HelpText = "Delete outputdirectory of results before running tests.")]
    public bool CleanOutputDirectory { get; set; }

    /// <summary>
    /// Disable console output
    /// </summary>
    [Option("no_console", HelpText = "Disables output to console.")]
    public bool DisableConsoleOutput { get; set; }

    /// <summary>
    /// DisableFileOutput
    /// </summary>
    [Option("no_file", HelpText = "Disables output to file. If this is set, -c has no effect.")]
    public bool DisableFileOutput { get; set; }

    /// <summary>
    /// Path to assembly with unit tests
    /// </summary>
    [Value(0, MetaName = "FilePaths", HelpText = "Files of assemblies to search for unit tests. If not specified the complete directory will be searched.")]
    public IEnumerable<string> FilePaths { get; set; }

    /// <summary>
    /// Hide search errors
    /// </summary>
    [Option("no_search_errors", HelpText = "Disables display of errors found during test discovery.")]
    public bool HideSearchErrors { get; set; }

    /// <summary>
    /// Returns a builder to create a new instance of the config
    /// </summary>
    /// <returns></returns>
    public static RunnerConfigurationBuilder New()
    {
      return new RunnerConfigurationBuilder();
    }
  }
}
