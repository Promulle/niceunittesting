﻿using System.Collections.Generic;
using System.Reflection;

namespace NiceUnitTesting.Running
{
  /// <summary>
  /// Builder for RunnerConfiguration
  /// </summary>
  public class RunnerConfigurationBuilder
  {
    private bool m_Wait = true;
    private bool m_Threaded = false;
    private bool m_Statistic = true;
    private bool m_CleanLogDirectory = false;
    private bool m_DisableFileOutput = false;
    private bool m_DisableConsoleOutput = false;
    private bool m_ShowSearchErrors = true;
    private readonly List<string> m_AssemblyFilePaths = new List<string>();

    /// <summary>
    /// If the runner should wait after execution of the tests
    /// </summary>
    /// <param name="i_Wait"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder ShouldWait(bool i_Wait)
    {
      m_Wait = i_Wait;
      return this;
    }

    /// <summary>
    /// If the runner should use multiple threads
    /// </summary>
    /// <param name="i_Threaded"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder UseThreads(bool i_Threaded)
    {
      m_Threaded = i_Threaded;
      return this;
    }

    /// <summary>
    /// If a runner statistic should be displayed
    /// </summary>
    /// <param name="i_Statistic"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder DisplayStatistic(bool i_Statistic)
    {
      m_Statistic = i_Statistic;
      return this;
    }

    /// <summary>
    /// Whether to clean the directory this tool will log the results to
    /// </summary>
    /// <param name="i_Clean"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder CleanLogDirectory(bool i_Clean)
    {
      m_CleanLogDirectory = i_Clean;
      return this;
    }

    /// <summary>
    /// If the results should be output to file at all
    /// </summary>
    /// <param name="i_Output"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder OutputToFile(bool i_Output)
    {
      m_DisableFileOutput = !i_Output;
      return this;
    }

    /// <summary>
    /// Whether the output should be shown on console
    /// </summary>
    /// <param name="i_Output"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder OutputToConsole(bool i_Output)
    {
      m_DisableConsoleOutput = !i_Output;
      return this;
    }

    /// <summary>
    /// Whether errors occuring during search for tests should be displayed
    /// </summary>
    /// <param name="i_Show"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder ShowSearchErrors(bool i_Show)
    {
      m_ShowSearchErrors = i_Show;
      return this;
    }

    /// <summary>
    /// Adds an assembly to the assemblies that should be searched for tests
    /// </summary>
    /// <param name="i_AssemblyFilePath"></param>
    /// <returns></returns>
    public RunnerConfigurationBuilder AddAssemblyWithTests(string i_AssemblyFilePath)
    {
      m_AssemblyFilePaths.Add(i_AssemblyFilePath);
      return this;
    }

    /// <summary>
    /// Adds this assembly when searching for tests
    /// </summary>
    /// <returns></returns>
    public RunnerConfigurationBuilder AddThisAssembly()
    {
      m_AssemblyFilePaths.Add(Assembly.GetEntryAssembly().Location);
      return this;
    }

    /// <summary>
    /// Creates a configuration from the settings
    /// </summary>
    /// <returns></returns>
    public RunnerConfiguration Create()
    {
      return new RunnerConfiguration()
      {
        CleanOutputDirectory = m_CleanLogDirectory,
        DisableConsoleOutput = m_DisableConsoleOutput,
        DisableFileOutput = m_DisableFileOutput,
        FilePaths = m_AssemblyFilePaths,
        Statistic = m_Statistic,
        Threaded = m_Threaded,
        Wait = m_Wait,
        HideSearchErrors = !m_ShowSearchErrors
      };
    }
  }
}
