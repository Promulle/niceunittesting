﻿using NiceUnitTesting.Tests.Execution.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NiceUnitTesting.Running
{
  /// <summary>
  /// Tool class that returns file paths of assemblies to scan for tests
  /// </summary>
  public static class SpecifiedAssemblies
  {
    /// <summary>
    /// Returns assemblies to scan
    /// </summary>
    /// <param name="i_Config"></param>
    /// <returns></returns>
    public static List<string> GetTestAssemblies(ITestingConfiguration i_Config)
    {
      var res = i_Config.FilePaths.ToList();

      if (res.Count == 0)
      {
        var dir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        Console.WriteLine(ProgramConstants.MESSAGE_NO_SPECIFIED + dir);
        res.AddRange(Directory.GetFiles(dir, ProgramConstants.PATTERN_DLL));
      }
      res.ForEach(r => Console.WriteLine($"Found test assembly: {r}"));
      return res;
    }
  }
}
