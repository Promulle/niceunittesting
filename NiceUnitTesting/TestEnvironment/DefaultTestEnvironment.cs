﻿using NiceUnitTesting.Tests;

namespace NiceUnitTesting.TestEnvironment
{
  /// <summary>
  /// Default impl of the testenvironment, this is the environment that is used for fixtures without specified Environment
  /// </summary>
  internal sealed class DefaultTestEnvironment : ITestEnvironment
  {
    private const string NAME = "Default";

    /// <summary>
    /// Name of the Environment
    /// </summary>
    public string Name => NAME;

    /// <summary>
    /// Enable threading
    /// </summary>
    public bool EnableThreading => false;

    /// <summary>
    /// is cacheable
    /// </summary>
    public bool IsCacheable => true;

    /// <summary>
    /// Nothing
    /// </summary>
    /// <param name="i_Sender"></param>
    /// <param name="i_Task"></param>
    public void OnTaskDone(object i_Sender, TestTask i_Task)
    {
    }

    /// <summary>
    /// Nothing
    /// </summary>
    /// <param name="i_Sender"></param>
    /// <param name="i_Test"></param>
    public void OnTestDone(object i_Sender, TestTask i_Test)
    {
    }

    /// <summary>
    /// Nothing
    /// </summary>
    /// <param name="i_Fixture"></param>
    public void Prepare(object i_Fixture)
    {
    }

    /// <summary>
    /// Nothing
    /// </summary>
    public void RunComplete()
    {
    }
  }
}
