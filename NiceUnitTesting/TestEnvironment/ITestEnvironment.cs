﻿using NiceUnitTesting.Tests;

namespace NiceUnitTesting.TestEnvironment
{
  /// <summary>
  /// Interface that should be implemented by environment classes
  /// </summary>
  public interface ITestEnvironment
  {
    /// <summary>
    /// Name of the environment
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Whether tests in this environment should be run by threads.
    /// CAUTION: tests should be isolated from eatch other for this to work properly.
    /// </summary>
    bool EnableThreading { get; }

    /// <summary>
    /// Whether this environment can be cached or not. Set this to false if you need state in the environment that not all Fixtures should be affected by
    /// </summary>
    bool IsCacheable { get; }

    /// <summary>
    /// Prepare fixture for running tests in this environment
    /// </summary>
    /// <param name="i_Fixture"></param>
    void Prepare(object i_Fixture);

    /// <summary>
    /// Function that gets called when every test has been executed, that should have been executed under this environment
    /// </summary>
    void RunComplete();

    /// <summary>
    /// Event handler for TestDone
    /// </summary>
    /// <param name="i_Test"></param>
    /// <param name="i_Sender"></param>
    void OnTestDone(object i_Sender, TestTask i_Test);

    /// <summary>
    /// Event handler for TaskDone
    /// </summary>
    /// <param name="i_Task"></param>
    /// <param name="i_Sender"></param>
    void OnTaskDone(object i_Sender, TestTask i_Task);
  }
}
