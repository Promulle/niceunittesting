﻿using NiceUnitTesting.Tests.Execution.Fixtures;
using System.Threading.Tasks;

namespace NiceUnitTesting.TestEnvironment
{
  /// <summary>
  /// Executor of an environment
  /// </summary>
  public class TestEnvironmentExecutor
  {
    /// <summary>
    /// Environment
    /// </summary>
    public ITestEnvironment Environment { get; }

    /// <summary>
    /// Environment Constructor
    /// </summary>
    public TestEnvironmentExecutor(ITestEnvironment i_Environment)
    {
      Environment = i_Environment;
    }

    /// <summary>
    /// Execute the tests from a fixture in the set Environment
    /// </summary>
    /// <param name="t_Fixture"></param>
    public async Task ExecuteFixture(TestRunFixture t_Fixture)
    {
      WireEventsFromRunner(t_Fixture);
      Environment.Prepare(t_Fixture.TestFixture);
      await t_Fixture.Runner.RunTests().ConfigureAwait(false);
      Environment.RunComplete();
      UnWireEventsFromRunner(t_Fixture);
    }

    private void WireEventsFromRunner(TestRunFixture t_Fixture)
    {
      t_Fixture.Runner.TestDone += Environment.OnTestDone;
      t_Fixture.Runner.TaskDone += Environment.OnTaskDone;
    }

    private void UnWireEventsFromRunner(TestRunFixture t_Fixture)
    {
      t_Fixture.Runner.TestDone -= Environment.OnTestDone;
      t_Fixture.Runner.TaskDone -= Environment.OnTaskDone;
    }
  }
}
