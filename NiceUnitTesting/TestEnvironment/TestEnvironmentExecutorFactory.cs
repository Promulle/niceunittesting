﻿using NiceUnitTesting.Base;

namespace NiceUnitTesting.TestEnvironment
{
  /// <summary>
  /// Factory for executors
  /// </summary>
  public static class TestEnvironmentExecutorFactory
  {
    private static readonly DefaultCache<ITestEnvironment, TestEnvironmentExecutor> m_ExecutorCache = new DefaultCache<ITestEnvironment, TestEnvironmentExecutor>(r => new TestEnvironmentExecutor(r));

    /// <summary>
    /// Returns a cached Executor or creates a new one if there is none for i_Environment
    /// </summary>
    /// <param name="i_Environment"></param>
    /// <returns></returns>
    public static TestEnvironmentExecutor CreateOrGetExecutor(ITestEnvironment i_Environment)
    {
      return m_ExecutorCache.GetOrCreate(i_Environment);
    }
  }
}
