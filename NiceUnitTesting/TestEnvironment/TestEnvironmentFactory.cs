﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NiceUnitTesting.TestEnvironment
{
  /// <summary>
  /// Factory for the creation of TestEnvironments
  /// </summary>
  public static class TestEnvironmentFactory
  {
    private static readonly Dictionary<Type, ITestEnvironment> m_CachedInstances;

    /// <summary>
    /// Static constructor
    /// </summary>
    static TestEnvironmentFactory()
    {
      m_CachedInstances = new Dictionary<Type, ITestEnvironment>();
    }

    /// <summary>
    /// Creates an environment or retrieves it from cache. Only Environments with IsCacheable will be cached.
    /// </summary>
    /// <param name="i_EnvironmentType"></param>
    /// <returns></returns>
    public static ITestEnvironment CreateOrGetEnvironment(Type i_EnvironmentType)
    {
      if (!m_CachedInstances.TryGetValue(i_EnvironmentType, out var env))
      {
        EnsureEnvironmentType(i_EnvironmentType);
        env = Activator.CreateInstance(i_EnvironmentType) as ITestEnvironment;
        if (env.IsCacheable)
        {
          m_CachedInstances.Add(i_EnvironmentType, env);
        }
      }
      return env;
    }

    /// <summary>
    /// Returns the default environment to run tests in
    /// </summary>
    /// <returns></returns>
    internal static ITestEnvironment GetDefaultEnvironment()
    {
      return CreateOrGetEnvironment(typeof(DefaultTestEnvironment));
    }

    private static void EnsureEnvironmentType(Type i_EnvironmentType)
    {
      if (!typeof(ITestEnvironment).IsAssignableFrom(i_EnvironmentType))
      {
        throw new ArgumentException(BuildArgumentError(i_EnvironmentType));
      }
    }

    private static string BuildArgumentError(Type i_EnvironmentType)
    {
      return new StringBuilder()
        .Append("Trying to create type ")
        .Append(i_EnvironmentType.FullName)
        .Append(" that does not implement ")
        .Append(typeof(ITestEnvironment).FullName)
        .Append(".")
        .ToString();
    }
  }
}
