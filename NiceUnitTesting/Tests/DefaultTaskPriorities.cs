﻿namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Constants for priorities
  /// </summary>
  public static class DefaultTaskPriorities
  {
    /// <summary>
    /// Priority assigned to crucial tasks that should run before anything else
    /// </summary>
    public static readonly int INFRASTRUCTUR = 0;

    /// <summary>
    /// Priority for tasks that actually do stuff
    /// </summary>
    public static readonly int ACTUAL_CASE = 1;
  }
}
