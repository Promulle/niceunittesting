﻿using System.Collections.Generic;

namespace NiceUnitTesting.Tests.Execution.Config
{
  /// <summary>
  /// Interface for configuration of tests
  /// </summary>
  public interface ITestingConfiguration
  {
    /// <summary>
    /// Whether the tester should wait
    /// </summary>
    bool Wait { get; }

    /// <summary>
    /// Whether to run threaded or not
    /// </summary>
    bool Threaded { get; }

    /// <summary>
    /// Whether search errors should be hidden or not
    /// </summary>
    bool HideSearchErrors { get; }

    /// <summary>
    /// Whether a statistic should be used
    /// </summary>
    bool Statistic { get; }

    /// <summary>
    /// Whether the outputdir should be cleaned before outputting files
    /// </summary>
    bool CleanOutputDirectory { get; }

    /// <summary>
    /// Whether Console Output should be disabled
    /// </summary>
    bool DisableConsoleOutput { get; }

    /// <summary>
    /// Whether File output should be disabled
    /// </summary>
    bool DisableFileOutput { get; }

    /// <summary>
    /// Path to assembly with unit tests
    /// </summary>
    IEnumerable<string> FilePaths { get; }
  }
}
