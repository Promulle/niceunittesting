﻿using NiceUnitTesting.TestEnvironment;
using NiceUnitTesting.Tests.Execution.Runners;

namespace NiceUnitTesting.Tests.Execution.Fixtures
{
  /// <summary>
  /// Fixture combining TestFixture and Runner
  /// </summary>
  public class TestRunFixture
  {
    /// <summary>
    /// The real object with methods
    /// </summary>
    public object TestFixture { get; set; }

    /// <summary>
    /// Runner for running the tests
    /// </summary>
    public TestRunner Runner { get; set; }

    /// <summary>
    /// Environment
    /// </summary>
    public ITestEnvironment TestEnvironment { get; set; }
  }
}
