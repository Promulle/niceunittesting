﻿using NiceUnitTesting.TestEnvironment;
using System;
using System.Collections.Generic;

namespace NiceUnitTesting.Tests.Execution.Fixtures
{
  /// <summary>
  /// Class representing a single TestFixture
  /// </summary>
  public class TestRunFixtureDefinition
  {
    /// <summary>
    /// Object holding test methods
    /// </summary>
    public Type TestFixtureType { get; set; }

    /// <summary>
    /// Definitions of tests
    /// </summary>
    public List<TestTaskDefinition> Definitions { get; } = new List<TestTaskDefinition>();

    /// <summary>
    /// Environments the fixture should be run in
    /// </summary>
    public List<ITestEnvironment> Environments { get; } = new List<ITestEnvironment>();

    /// <summary>
    /// Author of the TestFixture 
    /// -> will be used as author for tests run by this host if the test does not have its own author
    /// </summary>
    public string Author { get; set; } = TaskDefinitionFactory.NOT_AVAILABLE;
  }
}
