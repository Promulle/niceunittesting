﻿using NiceUnitTesting.TestEnvironment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceUnitTesting.Tests.Execution.Fixtures
{
  /// <summary>
  /// Factory for fixture definitions
  /// </summary>
  public static class TestRunFixtureDefinitionFactory
  {
    /// <summary>
    /// Creates a fixture definition from a type
    /// </summary>
    /// <param name="i_Type"></param>
    /// <returns></returns>
    public static TestRunFixtureDefinition CreateDefiniton(Type i_Type)
    {
      var res = new TestRunFixtureDefinition
      {
        TestFixtureType = i_Type,
      };
      res.Definitions.AddRange(GetTestTasksIn(i_Type, res.Author));
      res.Environments.AddRange(GetTestEnvironments(i_Type));
      return res;
    }

    /// <summary>
    /// Reads test tasks in i_FixtureType and assigns i_PossibleAuthor
    /// </summary>
    /// <param name="i_FixtureType"></param>
    /// <param name="i_PossibleAuthor"></param>
    /// <returns></returns>
    private static IEnumerable<TestTaskDefinition> GetTestTasksIn(Type i_FixtureType, string i_PossibleAuthor)
    {
      var res = new List<TestTaskDefinition>();
      foreach (var methodInfo in i_FixtureType.GetMethods())
      {
        var testDefinition = TaskDefinitionFactory.Create(methodInfo);
        //pass through author if not set by test
        if (testDefinition.Author == TaskDefinitionFactory.NOT_AVAILABLE && i_PossibleAuthor != null && i_PossibleAuthor != TaskDefinitionFactory.NOT_AVAILABLE)
        {
          testDefinition.Author = i_PossibleAuthor;
        }
        if (testDefinition.Mode != TaskMode.None)
        {
          res.Add(testDefinition);
        }
        if (testDefinition.Mode == TaskMode.Test)
        {
          Console.WriteLine("Found test: " + testDefinition.TestNameAndAuthor);
        }
      }
      return res;
    }

    private static IEnumerable<ITestEnvironment> GetTestEnvironments(Type i_Type)
    {
      var foundEnvironments = i_Type.GetCustomAttributes(true)
        .OfType<EnvironmentAttribute>()
        .Select(r => TestEnvironmentFactory.CreateOrGetEnvironment(r.ImplementationType))
        .Distinct();
      if (!foundEnvironments.Any())
      {
        return new[] { TestEnvironmentFactory.GetDefaultEnvironment() };
      }
      return foundEnvironments;
    }
  }
}

