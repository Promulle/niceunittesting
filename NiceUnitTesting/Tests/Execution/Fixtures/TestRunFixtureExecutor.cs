﻿using NiceUnitTesting.TestEnvironment;
using NiceUnitTesting.Tools;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceUnitTesting.Tests.Execution.Fixtures
{
  /// <summary>
  /// Executor for fixtures
  /// </summary>
  public class TestRunFixtureExecutor
  {
    /// <summary>
    /// Scheduler for threads
    /// </summary>
    public ThreadScheduler ThreadScheduler { get; } = new ThreadScheduler();

    /// <summary>
    /// All fixtures that were executed by this executor
    /// </summary>
    public IList<TestRunFixture> ExecutedFixtures { get; } = new List<TestRunFixture>();

    /// <summary>
    /// Runs fixtures
    /// </summary>
    /// <param name="i_Fixtures"></param>
    /// <param name="i_Threaded">Whether fixtures can be run threaded.</param>
    public async Task RunFixtures(IEnumerable<TestRunFixtureDefinition> i_Fixtures, bool i_Threaded)
    {
      if (i_Threaded)
      {
        foreach (var fixture in i_Fixtures)
        {
          ThreadScheduler.AddAutoThreaded(async () => await Run(fixture).ConfigureAwait(false));
        }
        ThreadScheduler.RunThreads();
      }
      else
      {
        foreach (var fixture in i_Fixtures)
        {
          await Run(fixture).ConfigureAwait(false);
        }
      }
    }

    private async Task Run(TestRunFixtureDefinition i_Definition)
    {
      foreach (var env in i_Definition.Environments)
      {
        var runFixture = TestRunFixtureFactory.Create(i_Definition, env);
        var executor = TestEnvironmentExecutorFactory.CreateOrGetExecutor(env);
        await executor.ExecuteFixture(runFixture);
        ExecutedFixtures.Add(runFixture);
      }
    }
  }
}
