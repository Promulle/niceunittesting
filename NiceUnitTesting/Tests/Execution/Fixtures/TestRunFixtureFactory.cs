﻿using NiceUnitTesting.TestEnvironment;
using NiceUnitTesting.Tests.Execution.Runners;
using NiceUnitTesting.Tests.Execution.Storage;
using System;

namespace NiceUnitTesting.Tests.Execution.Fixtures
{
  /// <summary>
  /// Factory for creation of TestRunFixture from Definition
  /// </summary>
  public static class TestRunFixtureFactory
  {
    /// <summary>
    /// Creates a new TestRunFixture from i_Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Environment"></param>
    /// <returns></returns>
    public static TestRunFixture Create(TestRunFixtureDefinition i_Definition, ITestEnvironment i_Environment)
    {
      var realFixture = Activator.CreateInstance(i_Definition.TestFixtureType);
      var runner = new TestRunner()
      {
        TestFixture = realFixture,
        Tasks = TaskStorageFactory.CreateTaskStorage(i_Definition.Definitions, realFixture, i_Environment),
      };
      return new TestRunFixture()
      {
        TestFixture = realFixture,
        Runner = runner,
        TestEnvironment = i_Environment,
      };
    }
  }
}
