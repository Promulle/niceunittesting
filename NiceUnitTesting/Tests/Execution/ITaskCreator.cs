﻿using NiceUnitTesting.TestEnvironment;
using System.Collections.Generic;

namespace NiceUnitTesting.Tests.Execution
{
  /// <summary>
  /// Creates tasks based on definitions
  /// </summary>
  public interface ITaskCreator
  {
    /// <summary>
    /// Create Tasks from i_Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Environment"></param>
    /// <returns></returns>
    IEnumerable<TestTask> CreateTasks(TestTaskDefinition i_Definition, object i_TestFixture, ITestEnvironment i_Environment);
  }
}