﻿using NiceUnitTesting.Tests.Execution.Storage;
using NiceUnitTesting.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NiceUnitTesting.Tests.Execution.Runners
{
  /// <summary>
  /// Queue for Tests
  /// </summary>
  public class TestRunner
  {
    private const string OUTPUT_RUNNING_TESTS = "Running Tests for: ";
    private const string OUTPUT_RUNNING_TESTS_IN = " in: ";

    /// <summary>
    /// Fixture to run tests on
    /// </summary>
    public object TestFixture { get; set; }

    /// <summary>
    /// Whether threads should be used
    /// </summary>
    public bool UseThreads { get; set; }

    /// <summary>
    /// Storage of Tasks
    /// </summary>
    public TaskStorage Tasks { get; set; }

    /// <summary>
    /// Event fired when a non-test task is done
    /// </summary>
    public event EventHandler<TestTask> TaskDone;

    /// <summary>
    /// Event fired when a test is done
    /// </summary>
    public event EventHandler<TestTask> TestDone;

    /// <summary>
    /// RunTests
    /// </summary>
    public async Task RunTests()
    {
      Console.WriteLine(OUTPUT_RUNNING_TESTS + TestFixture.GetType().Name + Environment.NewLine
        + OUTPUT_RUNNING_TESTS_IN + TestFixture.GetType().Assembly.GetName().Name);
      await RunTasks(Tasks.OneTimeSetups).ConfigureAwait(false);
      await RunAllTests().ConfigureAwait(false);
      await RunTasks(Tasks.OneTimeTeardowns).ConfigureAwait(false);
    }

    private async Task RunAllTests()
    {
      var testList = Tasks.Tests;
      if (UseThreads)
      {
        //collect all tests that will be run on main thread
        testList = Tasks.Tests.Where(r => r.Definition.ThreadingMode == ThreadModes.Main).ToList();
        //collect all tests that will run on separate threads
        var scheduler = new ThreadScheduler();
        foreach (var test in Tasks.Tests.Where(r => r.Definition.ThreadingMode == ThreadModes.Auto))
        {
          scheduler.AddAutoThreaded(async () => await RunTaskChain(test).ConfigureAwait(false));
        }
        scheduler.RunThreads();
      }
      foreach (var test in testList)
      {
        await RunTaskChain(test).ConfigureAwait(false);
      }
    }

    private async Task RunTaskChain(TestTask i_Task)
    {
      if (i_Task.Definition.UseSetups)
      {
        await RunTasks(Tasks.Setups).ConfigureAwait(false);
      }
      await RunTask(i_Task).ConfigureAwait(false);
      if (i_Task.Definition.UseTearDowns)
      {
        await RunTasks(Tasks.Teardowns).ConfigureAwait(false);
      }
    }

    private async Task RunTask(TestTask i_Task)
    {
      Console.WriteLine("Running Task: " + i_Task.Definition.TestNameAndAuthor);
      await i_Task.Run().ConfigureAwait(false);
      PostResults(i_Task);
    }

    private async Task RunTasks(List<TestTask> i_TaskList)
    {
      foreach (var task in i_TaskList)
      {
        await RunTask(task).ConfigureAwait(false);
      }
    }

    private void PostResults(TestTask i_Task)
    {
      RaiseTaskDone(i_Task);
      //if the task that has been run is a test
      if (i_Task.Definition.Mode == TaskMode.Test)
      {
        RaiseTestDone(i_Task);
      }
    }

    private void RaiseTestDone(TestTask i_Test)
    {
      TestDone?.Invoke(this, i_Test);
    }

    private void RaiseTaskDone(TestTask i_Task)
    {
      TaskDone?.Invoke(this, i_Task);
    }
  }
}
