﻿using System;

namespace NiceUnitTesting.Tests.Execution.Statistics
{
  /// <summary>
  /// Part for Failures
  /// </summary>
  public class FailurePart : StatisticsPart
  {
    /// <summary>
    /// Failure
    /// </summary>
    public override string PartName => "Failure";

    /// <summary>
    /// If failure
    /// </summary>
    public override Predicate<TestTaskResult> AcceptPredicate { get; } = r => r.WasSuccessful == false;
  }
}
