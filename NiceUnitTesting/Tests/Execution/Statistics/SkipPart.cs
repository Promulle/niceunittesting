﻿using System;

namespace NiceUnitTesting.Tests.Execution.Statistics
{
  /// <summary>
  /// Skipped tests
  /// </summary>
  public class SkipPart : StatisticsPart
  {
    /// <summary>
    /// Skipped
    /// </summary>
    public override string PartName => "Skipped";

    /// <summary>
    /// If skipped
    /// </summary>
    public override Predicate<TestTaskResult> AcceptPredicate { get; } = r => r.WasSuccessful == null;
  }
}
