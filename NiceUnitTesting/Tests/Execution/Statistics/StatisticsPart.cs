﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NiceUnitTesting.Tests.Execution.Statistics
{
  /// <summary>
  /// Part of the statistic
  /// </summary>
  public abstract class StatisticsPart
  {
    /// <summary>
    /// All results of this part
    /// </summary>
    public List<TestTaskResult> Results { get; } = new List<TestTaskResult>();

    /// <summary>
    /// Predicate determining whether a result is accepted
    /// </summary>
    public abstract Predicate<TestTaskResult> AcceptPredicate { get; }

    /// <summary>
    /// Name of the part
    /// </summary>
    public abstract string PartName { get; }

    /// <summary>
    /// Count
    /// </summary>
    public int Count
    {
      get { return Results.Count; }
    }

    /// <summary>
    /// Checks whether i_Result should be booked under this part
    /// </summary>
    /// <param name="i_Result"></param>
    /// <returns></returns>
    public bool AddIfFitting(TestTaskResult i_Result)
    {
      if (AcceptPredicate(i_Result))
      {
        AddResult(i_Result);
        return true;
      }
      return false;
    }

    /// <summary>
    /// Adds result and increments count
    /// </summary>
    /// <param name="i_Result"></param>
    protected void AddResult(TestTaskResult i_Result)
    {
      Results.Add(i_Result);
    }

    /// <summary>
    /// ToString
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return new StringBuilder()
        .Append(PartName)
        .Append(": ")
        .Append(Count)
        .ToString();
    }
  }
}
