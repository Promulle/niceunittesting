﻿using System;

namespace NiceUnitTesting.Tests.Execution.Statistics
{
  /// <summary>
  /// part for success
  /// </summary>
  public class SuccessPart : StatisticsPart
  {
    /// <summary>
    /// Name of Success
    /// </summary>
    public override string PartName => "Success";

    /// <summary>
    /// If Success
    /// </summary>
    public override Predicate<TestTaskResult> AcceptPredicate { get; } = r => r.WasSuccessful == true;
  }
}
