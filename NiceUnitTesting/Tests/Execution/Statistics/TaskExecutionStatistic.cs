﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NiceUnitTesting.Tests.Execution.Statistics
{
  /// <summary>
  /// Statistic of executed Tests
  /// </summary>
  public class TaskExecutionStatistic
  {
    /// <summary>
    /// parts of the statistic
    /// </summary>
    public List<StatisticsPart> Parts { get; } = new List<StatisticsPart>()
    {
      new SuccessPart(),
      new FailurePart(),
      new SkipPart(),
    };

    /// <summary>
    /// Adds all results from i_Results to this statistic
    /// </summary>
    /// <param name="i_Results"></param>
    public void AddToStatistic(IEnumerable<TestTaskResult> i_Results)
    {
      foreach (var res in i_Results)
      {
        var isTaken = false;
        for (var i = 0; i < Parts.Count && !isTaken; i++)
        {
          isTaken = Parts[i].AddIfFitting(res);
        }
      }
    }

    /// <summary>
    /// Prepares a list of displayable lines
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> ToDisplayLines()
    {
      var builder = new StringBuilder()
        .Append("[SUMMARY]: Total: ")
        .Append(GetTestCount())
        .Append(" ");
      foreach (var part in Parts)
      {
        if (part.Count > 0)
        {
          builder.Append(part.ToString())
            .Append(" ");
        }
      }
      return builder.ToString()
        .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
    }

    private int GetTestCount()
    {
      return Parts.Sum(r => r.Count);
    }
  }
}
