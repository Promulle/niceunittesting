﻿using NiceUnitTesting.TestEnvironment;
using System.Collections.Generic;

namespace NiceUnitTesting.Tests.Execution.Storage
{
  /// <summary>
  /// Storage for all tasks of a runner
  /// </summary>
  public class TaskStorage
  {
    /// <summary>
    /// Setup Tests
    /// </summary>
    public List<TestTask> Setups { get; } = new List<TestTask>();

    /// <summary>
    /// Setups running once
    /// </summary>
    public List<TestTask> OneTimeSetups { get; } = new List<TestTask>();

    /// <summary>
    /// Tests
    /// </summary>
    public List<TestTask> Tests { get; } = new List<TestTask>();

    /// <summary>
    /// teardowns after tests
    /// </summary>
    public List<TestTask> Teardowns { get; } = new List<TestTask>();

    /// <summary>
    /// teardowns after tests
    /// </summary>
    public List<TestTask> OneTimeTeardowns { get; } = new List<TestTask>();

    /// <summary>
    /// Sorts test into the appropriate TaskList
    /// </summary>
    /// <param name="i_TestMethod"></param>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Creator"></param>
    /// <param name="i_Environment"></param>
    public void SortTest(TestTaskDefinition i_TestMethod, object i_TestFixture, ITaskCreator i_Creator, ITestEnvironment i_Environment)
    {
      List<TestTask> list = null;
      if (i_TestMethod.Mode == TaskMode.OneTimeSetup)
      {
        list = OneTimeSetups;
      }
      else if (i_TestMethod.Mode == TaskMode.Setup)
      {
        list = Setups;
      }
      else if (i_TestMethod.Mode == TaskMode.Teardown)
      {
        list = Teardowns;
      }
      else if (i_TestMethod.Mode == TaskMode.OneTimeTeardown)
      {
        list = OneTimeTeardowns;
      }
      else if (i_TestMethod.Mode == TaskMode.Test)
      {
        list = Tests;
      }
      AddTasksByDefinition(i_TestMethod, i_Creator, i_TestFixture, i_Environment, list);
    }

    /// <summary>
    /// Clears the storage
    /// </summary>
    public void Clear()
    {
      Tests.Clear();
      Setups.Clear();
      OneTimeSetups.Clear();
      Teardowns.Clear();
      OneTimeTeardowns.Clear();
    }

    private void AddTasksByDefinition(TestTaskDefinition i_Definition, ITaskCreator i_Creator, object i_TestFixture, ITestEnvironment i_Environment, List<TestTask> t_Collection)
    {
      t_Collection?.AddRange(i_Creator.CreateTasks(i_Definition, i_TestFixture, i_Environment));
    }
  }
}
