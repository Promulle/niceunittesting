﻿using NiceUnitTesting.TestEnvironment;
using System.Collections.Generic;
using System.Linq;

namespace NiceUnitTesting.Tests.Execution.Storage
{
  /// <summary>
  /// Factory for TaskStorage
  /// </summary>
  public static class TaskStorageFactory
  {
    /// <summary>
    /// Creates a taskstorage
    /// </summary>
    /// <param name="i_Definitions"></param>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Environment"></param>
    /// <returns></returns>
    public static TaskStorage CreateTaskStorage(IEnumerable<TestTaskDefinition> i_Definitions, object i_TestFixture, ITestEnvironment i_Environment)
    {
      var storage = new TaskStorage();
      var creator = new TaskCreator();
      foreach (var def in i_Definitions.OrderBy(r => r.Priority))
      {
        storage.SortTest(def, i_TestFixture, creator, i_Environment);
      }
      return storage;
    }
  }
}
