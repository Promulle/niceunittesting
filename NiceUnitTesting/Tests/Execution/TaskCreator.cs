﻿using NiceUnitTesting.TestEnvironment;
using System.Collections.Generic;
using System.Linq;

namespace NiceUnitTesting.Tests.Execution
{
  /// <summary>
  /// Creates tasks from Definitions
  /// </summary>
  public class TaskCreator : ITaskCreator
  {
    /// <summary>
    /// Create Tasks from Definition
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Environment"></param>
    /// <returns></returns>
    public virtual IEnumerable<TestTask> CreateTasks(TestTaskDefinition i_Definition, object i_TestFixture, ITestEnvironment i_Environment)
    {
      if (i_Definition.Cases.Count > 0)
      {
        return i_Definition.Cases
          .Select(r => new ParameterizedTestTask(r, i_TestFixture, i_Environment)
          {
            Parameters = r.Parameters,
          }
          );
      }
      else
      {
        return new List<TestTask>() { new TestTask(i_Definition, i_TestFixture, i_Environment) };
      }
    }
  }
}
