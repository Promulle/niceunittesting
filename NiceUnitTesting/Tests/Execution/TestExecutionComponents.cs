﻿using NiceUnitTesting.Tests.Execution.Config;
using NiceUnitTesting.Tests.Execution.Statistics;
using NiceUnitTesting.Tests.Output;

namespace NiceUnitTesting.Tests.Execution
{
  /// <summary>
  /// Singleton for Execution components
  /// </summary>
  public sealed class TestExecutionComponents
  {
    private static TestExecutionComponents m_Instance;

    /// <summary>
    /// Returns the instance
    /// </summary>
    /// <param name="i_Config"></param>
    /// <returns></returns>
    public static TestExecutionComponents GetComponents(ITestingConfiguration i_Config)
    {
      if (m_Instance == null)
      {
        m_Instance = new TestExecutionComponents(i_Config);
      }
      return m_Instance;
    }

    /// <summary>
    /// Statistic
    /// </summary>
    public TaskExecutionStatistic Statistic { get; private set; }

    /// <summary>
    /// Output
    /// </summary>
    public TestOutput Output { get; private set; }

    private TestExecutionComponents(ITestingConfiguration i_Config)
    {
      InitComponents(i_Config);
    }

    private void InitComponents(ITestingConfiguration i_Config)
    {
      if (i_Config.Statistic)
      {
        Statistic = new TaskExecutionStatistic();
      }
      Output = new TestOutput(i_Config);
    }
  }
}
