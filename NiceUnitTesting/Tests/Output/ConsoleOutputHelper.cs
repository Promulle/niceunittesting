﻿using System;

namespace NiceUnitTesting.Tests.Output
{
  /// <summary>
  /// Outputhelper to console
  /// </summary>
  public static class ConsoleOutputHelper
  {
    /// <summary>
    /// Outputs to console
    /// </summary>
    /// <param name="i_TestResult"></param>
    /// <param name="i_Color"></param>
    public static void OutputConsole(string i_TestResult, ConsoleColor i_Color)
    {
      Console.ForegroundColor = i_Color;
      Console.WriteLine(i_TestResult);
      Console.ResetColor();
    }
  }
}
