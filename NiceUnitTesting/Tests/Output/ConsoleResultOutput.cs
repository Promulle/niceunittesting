﻿using System;
using System.Text;

namespace NiceUnitTesting.Tests.Output
{
  /// <summary>
  /// Output to console
  /// </summary>
  public class ConsoleResultOutput
  {
    private const string OUTPUT_TEST_NAME = "Test: ";
    private const string OUTPUT_TEST_FAIL = " has failed during execute.";
    private const string OUTPUT_TEST_SUCCESS = " has run successfully.";
    private const string OUTPUT_TEST_NONE = " has not been run";

    /// <summary>
    /// Outptu to console
    /// </summary>
    /// <param name="i_Result"></param>
    /// <returns>true</returns>
    public bool OutputResult(TestTaskResult i_Result)
    {
      var textColor = ConsoleColor.White;
      var resultBuilder = new StringBuilder();
      resultBuilder.Append(OUTPUT_TEST_NAME);
      //NameOfTest includes author if provided
      resultBuilder.Append(i_Result.NameOfTest);
      if (i_Result.WasSuccessful == true)
      {
        resultBuilder.Append(OUTPUT_TEST_SUCCESS);
        textColor = ConsoleColor.Green;
      }
      else if (i_Result.WasSuccessful == false)
      {
        resultBuilder.AppendLine(OUTPUT_TEST_FAIL);
        resultBuilder.Append(i_Result.ErrorMessage);
        textColor = ConsoleColor.Red;
      }
      else
      {
        resultBuilder.Append(OUTPUT_TEST_NONE);
      }
      var textResult = resultBuilder + "";
      ConsoleOutputHelper.OutputConsole(textResult, textColor);
      return true;
    }

    /// <summary>
    /// Output somehting to console in i_Color
    /// </summary>
    /// <param name="i_Value"></param>
    /// <param name="i_Color"></param>
    public void Output(string i_Value, ConsoleColor i_Color)
    {
      Console.ForegroundColor = i_Color;
      Console.WriteLine(i_Value);
      Console.ResetColor();
    }

    /// <summary>
    /// Output somehting to console
    /// </summary>
    /// <param name="i_Value"></param>
    public void Output(string i_Value)
    {
      Console.WriteLine(i_Value);
    }
  }
}
