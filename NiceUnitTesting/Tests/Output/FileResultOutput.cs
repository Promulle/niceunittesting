﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace NiceUnitTesting.Tests.Output
{
  /// <summary>
  /// Output to file
  /// </summary>
  public class FileResultOutput
  {
    private const string OUTPUT_FILE_BASE_NAME = "TestResults_";
    private const string OUTPUT_EXTENSION = ".xml";
    private const string DEFAULT_DIR_NAME = "TestResultOutput";

    /// <summary>
    /// Directory to output
    /// </summary>
    public string OutputDirectory { get; }

    /// <summary>
    /// Constructor with clean argument
    /// </summary>
    /// <param name="i_Clean"></param>
    public FileResultOutput(bool i_Clean)
    {
      var startDir = Path.GetDirectoryName(typeof(TestOutput).Assembly.Location);
      OutputDirectory = startDir + Path.DirectorySeparatorChar + DEFAULT_DIR_NAME;
      if (i_Clean)
      {
        CleanOutputDir();
      }
    }

    private void CleanOutputDir()
    {
      if (Directory.Exists(OutputDirectory))
      {
        try
        {
          Directory.Delete(OutputDirectory, true);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Error cleaning directory " + OutputDirectory + Environment.NewLine + ex);
        }
      }
    }

    /// <summary>
    /// Output i_Results to file
    /// </summary>
    /// <param name="i_Results"></param>
    /// <param name="i_HostName"></param>
    public void OutputXmlFile(List<TestTaskResult> i_Results, string i_HostName)
    {
      OutputXmlFile(i_Results, OutputDirectory, i_HostName);
    }

    /// <summary>
    /// Output i_Results to file
    /// </summary>
    /// <param name="i_Results"></param>
    /// <param name="i_OutputDir"></param>
    /// <param name="i_HostName"></param>
    public void OutputXmlFile(List<TestTaskResult> i_Results, string i_OutputDir, string i_HostName)
    {
      Directory.CreateDirectory(i_OutputDir);
      var filePath = BuildFileName(i_HostName, i_OutputDir);
      var xmlSerializer = new XmlSerializer(i_Results.GetType());
      try
      {
        using (var fileStream = File.OpenWrite(filePath))
        {
          xmlSerializer.Serialize(fileStream, i_Results);
        }
      }
      catch (Exception ex)
      {
        ConsoleOutputHelper.OutputConsole("Error writing to file: " + filePath + Environment.NewLine + ex, ConsoleColor.Yellow);
      }
    }

    private string BuildFileName(string i_HostName, string i_OutputDir)
    {
      var curTime = DateTime.Now;
      return new StringBuilder()
        .Append(i_OutputDir)
        .Append(Path.DirectorySeparatorChar)
        .Append(OUTPUT_FILE_BASE_NAME)
        .Append(i_HostName)
        .Append("_")
        .Append(curTime.Year)
        .Append(curTime.Month)
        .Append(curTime.Day)
        .Append(curTime.Minute)
        .Append(curTime.Second)
        .Append(OUTPUT_EXTENSION)
        .ToString();
    }
  }
}
