﻿using NiceUnitTesting.Tests.Execution.Config;
using NiceUnitTesting.Tests.Execution.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NiceUnitTesting.Tests.Output
{
  /// <summary>
  /// Class dealing with outputting testresults
  /// </summary>
  public class TestOutput
  {
    /// <summary>
    /// Console output
    /// </summary>
    public ConsoleResultOutput ConsoleOutput { get; }

    /// <summary>
    /// Output to file
    /// </summary>
    public FileResultOutput FileOutput { get; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="i_Config"></param>
    public TestOutput(ITestingConfiguration i_Config)
    {
      if (!i_Config.DisableConsoleOutput)
      {
        ConsoleOutput = new ConsoleResultOutput();
      }
      if (!i_Config.DisableFileOutput)
      {
        FileOutput = new FileResultOutput(i_Config.CleanOutputDirectory);
      }
    }

    /// <summary>
    /// Outputs all added results
    /// </summary>
    public void OutputResults(IEnumerable<TestTaskResult> i_Results)
    {
      ConsoleOutput?.Output("");
      ConsoleOutput?.Output("############# Results #############");
      foreach (var hostGroup in i_Results.GroupBy(r => r.NameOfFixture))
      {
        ConsoleOutput?.Output("Results for fixture: " + hostGroup.Key);
        var hostList = hostGroup.ToList();
        foreach (var envGroup in hostList.GroupBy(r => r.NameOfEnvironment))
        {
          var byEnv = envGroup.ToList();
          ConsoleOutput?.Output("\tInEnvironment: " + envGroup.Key);
          byEnv.ForEach(r => ConsoleOutput.OutputResult(r));
        }
        ConsoleOutput?.Output("");
        FileOutput?.OutputXmlFile(hostList, hostGroup.Key);
      }
    }

    /// <summary>
    /// Outputs the statistic to console and file
    /// </summary>
    /// <param name="i_Statistic"></param>
    public void OutputStatistic(TaskExecutionStatistic i_Statistic)
    {
      var toDisplay = i_Statistic.ToDisplayLines();
      toDisplay.ToList()
        .ForEach(r => ConsoleOutputHelper.OutputConsole(r, ConsoleColor.Cyan));
    }
  }
}
