﻿using NiceUnitTesting.TestEnvironment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// CoreTask that invokes the method with parameters
  /// </summary>
  public class ParameterizedTestTask : TestTask
  {
    /// <summary>
    /// Parameters to use
    /// </summary>
    public List<object> Parameters { get; set; }

    /// <summary>
    /// FillConstructor
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Fixture"></param>
    /// <param name="i_Environment"></param>
    public ParameterizedTestTask(TestTaskDefinition i_Definition, object i_Fixture, ITestEnvironment i_Environment)
      : base(i_Definition, i_Fixture, i_Environment)
    {
    }

    /// <summary>
    /// Invokes the test method with parameters
    /// </summary>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Definition"></param>
    protected override async Task InvokeTestMethod(object i_TestFixture, TestTaskDefinition i_Definition)
    {
      var res = Parameters != null ? Definition.Method.Invoke(i_TestFixture, Parameters.ToArray()) : base.InvokeTestMethod(i_TestFixture, i_Definition);
      if (res is Task casted)
      {
        await casted.ConfigureAwait(false);
      }
    }
  }
}
