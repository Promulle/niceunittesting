﻿using NiceUnitTesting.Tests.Execution.Fixtures;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace NiceUnitTesting.Tests.Search
{
  /// <summary>
  /// Class to discover tests
  /// </summary>
  public static class TestDiscovery
  {
    /// <summary>
    /// Searches the Assembly in i_FilePath for Tests
    /// </summary>
    /// <param name="i_FilePath"></param>
    /// <param name="i_HideErrors"></param>
    /// <returns></returns>
    public static List<TestRunFixtureDefinition> SearchAssemblyForTests(string i_FilePath, bool i_HideErrors)
    {
      var res = new List<TestRunFixtureDefinition>();
      try
      {
        var assembly = Assembly.LoadFrom(i_FilePath);
        if (assembly != null)
        {
          foreach (var type in assembly.GetTypes())
          {
            var host = SearchTypeForTest(type);
            if (host != null)
            {
              res.Add(host);
            }
          }
        }
      }
      catch (Exception ex)
      {
        if (!i_HideErrors)
        {
          Console.ForegroundColor = ConsoleColor.Yellow;
          Console.WriteLine(BuildSearchError(i_FilePath));
          Console.WriteLine(ex);
          if (ex is ReflectionTypeLoadException refLoadEx)
          {
            foreach (var refEx in refLoadEx.LoaderExceptions)
            {
              Console.WriteLine(refEx);
            }
          }
          Console.ResetColor();
        }
      }
      return res;
    }

    private static string BuildSearchError(string i_FilePath)
    {
      return new StringBuilder()
        .Append("Error searching ")
        .Append(i_FilePath)
        .Append(" for tests.")
        .ToString();
    }

    /// <summary>
    /// Searches a type for tests
    /// </summary>
    /// <param name="i_Type"></param>
    private static TestRunFixtureDefinition SearchTypeForTest(Type i_Type)
    {
      //only construct a fixture for non abstract types, because otherwise would fail
      if (!i_Type.IsAbstract && Attribute.GetCustomAttribute(i_Type, typeof(TestClassAttribute)) != null)
      {
        return TestRunFixtureDefinitionFactory.CreateDefiniton(i_Type);
      }
      return null;
    }
  }
}
