﻿using NiceUnitTesting.Attributes.Infrastructure;
using System;
using System.Reflection;

namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Factory that creates TaskDefinitions from Methods
  /// </summary>
  public static class TaskDefinitionFactory
  {
    /// <summary>
    /// Constant for not existing values, used for author or other optional entries
    /// </summary>
    public const string NOT_AVAILABLE = "Not available";

    /// <summary>
    /// Creates a TestTaskDefinition from i_Method
    /// </summary>
    /// <param name="i_Method"></param>
    /// <returns></returns>
    public static TestTaskDefinition Create(MethodInfo i_Method)
    {
      return ReadBaseDefinitionFromMethod<TestTaskDefinition>(i_Method, true);
    }

    /// <summary>
    /// Create a case definition
    /// </summary>
    /// <param name="i_Method"></param>
    /// <param name="i_CaseAttr"></param>
    /// <returns></returns>
    public static TestTaskParameterCaseDefinition CreateCaseDefinition(MethodInfo i_Method, TestCaseAttribute i_CaseAttr)
    {
      var caseDef = ReadBaseDefinitionFromMethod<TestTaskParameterCaseDefinition>(i_Method, false);
      caseDef.Parameters.AddRange(i_CaseAttr.Parameters);
      caseDef.CaseName = i_CaseAttr.Name;
      caseDef.UseSetups = i_CaseAttr.UseSetups;
      caseDef.UseTearDowns = i_CaseAttr.UseTearDowns;
      return caseDef;
    }

    /// <summary>
    /// Reads needed properties from method
    /// </summary>
    /// <param name="i_Info"></param>
    /// <param name="i_AllowCase"></param>
    private static T ReadBaseDefinitionFromMethod<T>(MethodInfo i_Info, bool i_AllowCase) where T : TestTaskDefinition
    {
      var def = Activator.CreateInstance<T>();
      def.Method = i_Info;
      def.Author = NOT_AVAILABLE;
      foreach (var attr in def.Method.GetCustomAttributes())
      {
        if (attr is PrioritizableTaskAttribute priorityAttr)
        {
          def.Priority = priorityAttr.Priority;
        }
        if (attr is SetupAttribute)
        {
          def.Mode = TaskMode.Setup;
        }
        else if (attr is OneTimeSetupAttribute)
        {
          def.Mode = TaskMode.OneTimeSetup;
        }
        else if (attr is TestAttribute testAttr)
        {
          def.Mode = TaskMode.Test;
          def.UseSetups = testAttr.UseSetups;
          def.UseTearDowns = testAttr.UseTearDowns;
        }
        else if (attr is TearDownAttribute)
        {
          def.Mode = TaskMode.Teardown;
        }
        else if (attr is OneTimeTearDownAttribute)
        {
          def.Mode = TaskMode.OneTimeTeardown;
        }
        else if (attr is AuthorAttribute authorAttr)
        {
          def.Author = GetAuthorFromAttribute(authorAttr);
        }
        else if (attr is TestThreadingAttribute threadAttr)
        {
          def.ThreadingMode = threadAttr.ThreadMode;
        }
        else if (attr is TestCaseAttribute caseAttr && i_AllowCase)
        {
          def.Cases.Add(CreateCaseDefinition(def.Method, caseAttr));
        }
      }
      def.TestName = "Test_" + def.Method.Name;
      return def;
    }

    /// <summary>
    /// Reads author from author attribute
    /// </summary>
    /// <param name="i_Attribute"></param>
    /// <returns></returns>
    private static string GetAuthorFromAttribute(AuthorAttribute i_Attribute)
    {
      if (i_Attribute?.Name != null)
      {
        return i_Attribute.Name;
      }
      return NOT_AVAILABLE;
    }
  }
}
