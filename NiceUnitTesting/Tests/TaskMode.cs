﻿namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Enum representing different kinds of tasks represented by methods
  /// </summary>
  public enum TaskMode
  {
    /// <summary>
    /// No test found
    /// </summary>
    None,

    /// <summary>
    /// Test
    /// </summary>
    Test,

    /// <summary>
    /// Setup running before every method
    /// </summary>
    Setup,

    /// <summary>
    /// Setup running once
    /// </summary>
    OneTimeSetup,

    /// <summary>
    /// Teardown running everytime
    /// </summary>
    Teardown,

    /// <summary>
    /// Teardown running once
    /// </summary>
    OneTimeTeardown,
  }
}
