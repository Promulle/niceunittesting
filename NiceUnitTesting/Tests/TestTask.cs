﻿using NiceUnitTesting.TestEnvironment;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Class representing an actual single test
  /// </summary>
  public class TestTask
  {
    private const string ENV_DEFAULT = "Default";
    private const string ENV_UNSPECIFIED = "Unspecified";

    /// <summary>
    /// The environment this test was run in
    /// </summary>
    public ITestEnvironment Environment { get; set; }

    /// <summary>
    /// Host class of tests
    /// </summary>
    public object TestFixture { get; set; }

    /// <summary>
    /// Definition behind this test
    /// </summary>
    public TestTaskDefinition Definition { get; protected set; }

    /// <summary>
    /// Result of the executed task
    /// </summary>
    public TestTaskResult Result { get; protected set; }

    /// <summary>
    /// Constructor setting Method
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <param name="i_Fixture"></param>
    /// <param name="i_Environment"></param>
    public TestTask(TestTaskDefinition i_Definition, object i_Fixture, ITestEnvironment i_Environment)
    {
      Definition = i_Definition;
      TestFixture = i_Fixture;
      Environment = i_Environment;
      Result = new TestTaskResult()
      {
        ThreadId = Thread.CurrentThread.ManagedThreadId,
        NameOfTest = i_Definition.TestNameAndAuthor,
        NameOfFixture = i_Fixture.GetType().FullName,
        NameOfEnvironment = GetCorrectEnvironmentName(i_Environment),
      };
    }

    private string GetCorrectEnvironmentName(ITestEnvironment i_Environment)
    {
      if (i_Environment != null)
      {
        if (i_Environment.Name != null)
        {
          return i_Environment.Name;
        }
        return ENV_UNSPECIFIED;
      }
      return ENV_DEFAULT;
    }

    /// <summary>
    /// Executes the method
    /// </summary>
    public virtual Task Run()
    {
      return RunByDefinition(Definition);
    }

    /// <summary>
    /// Executes i_Definition as test
    /// </summary>
    /// <param name="i_Definition"></param>
    /// <returns></returns>
    protected async Task<TestTaskResult> RunByDefinition(TestTaskDefinition i_Definition)
    {
      Result.WasSuccessful = null;
      try
      {
        await InvokeTestMethod(TestFixture, i_Definition).ConfigureAwait(false);
        Result.WasSuccessful = true;
      }
      catch (TargetInvocationException ex)
      {
        Result.WasSuccessful = false;
        Result.ErrorMessage = ex.InnerException.ToString();
      }
      catch (Exception ex)
      {
        Result.WasSuccessful = false;
        Result.ErrorMessage = ex.ToString();
      }
      return Result;
    }

    /// <summary>
    /// Invokes the testmethod
    /// </summary>
    /// <param name="i_TestFixture"></param>
    /// <param name="i_Definition"></param>
    protected virtual async Task InvokeTestMethod(object i_TestFixture, TestTaskDefinition i_Definition)
    {
      var res = i_Definition.Method.Invoke(i_TestFixture, null);
      if (res is Task casted)
      {
        await casted.ConfigureAwait(false);
      }
    }
  }
}
