﻿using System.Collections.Generic;
using System.Reflection;

namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Definition of a test to run.
  /// a single test can be created using this definiton. This enables the tests to know their Fixture
  /// but also allows for a per environment execution
  /// </summary>
  public class TestTaskDefinition
  {
    /// <summary>
    /// Mode of this TestMethod
    /// </summary>
    public TaskMode Mode { get; set; }

    /// <summary>
    /// Priority to run this task. Is not used for tests.
    /// </summary>
    public int Priority { get; set; }

    /// <summary>
    /// Mode for threading. Default: ThreadModes.Auto
    /// </summary>
    public ThreadModes ThreadingMode { get; set; }

    /// <summary>
    /// whether all setups should be run for this task
    /// OneTimeSetups are NOT affected.
    /// Default: true
    /// </summary>
    public bool UseSetups { get; set; }

    /// <summary>
    /// Whether all teardowns should be run for this task
    /// OneTimeTearDowns are NOT affected.
    /// Default: true
    /// </summary>
    public bool UseTearDowns { get; set; }

    /// <summary>
    /// Author of test. Default: NOT_AVAILABLE
    /// </summary>
    public virtual string Author { get; set; }

    /// <summary>
    /// Name of Test
    /// </summary>
    public virtual string TestName { get; set; }

    /// <summary>
    /// Returns TestName + Author if an author is set
    /// </summary>
    public virtual string TestNameAndAuthor
    {
      get
      {
        if (Author == TaskDefinitionFactory.NOT_AVAILABLE || Author == null)
        {
          return TestName;
        }
        else
        {
          return TestName + " (by " + Author + ")";
        }
      }
    }

    /// <summary>
    /// All cases
    /// </summary>
    public List<TestTaskParameterCaseDefinition> Cases { get; } = new List<TestTaskParameterCaseDefinition>();

    /// <summary>
    /// Method to execute Test
    /// </summary>
    public MethodInfo Method { get; set; }
  }
}
