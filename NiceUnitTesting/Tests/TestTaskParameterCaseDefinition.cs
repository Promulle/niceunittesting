﻿using System.Collections.Generic;
using System.Text;

namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// Definition used for parameterized cases -> outputs correct name
  /// </summary>
  public class TestTaskParameterCaseDefinition : TestTaskDefinition
  {
    private const string CASE_APPEND = " in case: ";

    /// <summary>
    /// Name of the case of these parameters. 
    /// If not set directly, a summary of parameters with types will be used
    /// </summary>
    public string CaseName { get; set; }

    /// <summary>
    /// Parameters
    /// </summary>
    public List<object> Parameters { get; } = new List<object>();

    /// <summary>
    /// override that returns correct name
    /// </summary>
    public override string TestNameAndAuthor
    {
      get
      {
        var origName = base.TestNameAndAuthor;
        var casedName = origName + CASE_APPEND;
        var nameOfCase = CaseName;
        if (nameOfCase == null)
        {
          nameOfCase = GenerateCaseName();
        }
        return casedName + nameOfCase;
      }
    }

    private string GenerateCaseName()
    {
      var builder = new StringBuilder();
      var parameterTypes = Method.GetParameters();
      builder.Append("( ");
      for (var i = 0; i < parameterTypes.Length; i++)
      {
        var curParam = parameterTypes[i];
        var curParamValue = Parameters[i];
        builder.Append(curParamValue);
        builder.Append(": ");
        builder.Append(curParam.ParameterType);
        if (i < parameterTypes.Length - 1)
        {
          builder.Append(", ");
        }
      }
      builder.Append(" )");
      return builder + "";
    }
  }
}
