﻿namespace NiceUnitTesting.Tests
{
  /// <summary>
  /// represents a result of a task
  /// </summary>
  public class TestTaskResult
  {
    /// <summary>
    /// Id of thread this has been run in
    /// </summary>
    public int ThreadId { get; set; }

    /// <summary>
    /// Environment this result has been run in
    /// </summary>
    public string NameOfEnvironment { get; set; }

    /// <summary>
    /// Name of the fixture this result has been run on
    /// </summary>
    public string NameOfFixture { get; set; }

    /// <summary>
    /// Name of the test this is a result of
    /// </summary>
    public string NameOfTest { get; set; }

    /// <summary>
    /// Whether this Test has been run successfully
    /// </summary>
    public bool? WasSuccessful { get; set; }

    /// <summary>
    /// Message of Exception, null when none occurred
    /// </summary>
    public string ErrorMessage { get; set; }
  }
}
