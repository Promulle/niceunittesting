﻿namespace NiceUnitTesting
{
  /// <summary>
  /// Modes for Threading of tests
  /// </summary>
  public enum ThreadModes
  {
    /// <summary>
    /// Default, threading decided by environment
    /// </summary>
    Auto,

    /// <summary>
    /// This test should be run on the main thread
    /// -> suitable for tests that are accessing directories/files and may collide with other tests if run in Auto mode
    /// </summary>
    Main,
  }
}
