﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NiceUnitTesting.Tools
{
  /// <summary>
  /// Class that manages threads and runs them
  /// </summary>
  public class ThreadScheduler
  {
    /// <summary>
    /// Threads for methods
    /// </summary>
    protected readonly List<Thread> m_Threads = new List<Thread>();

    /// <summary>
    /// Whether the scheduled threads have been started
    /// </summary>
    public bool AreThreadsRunning { get; protected set; }

    /// <summary>
    /// Adds a method to be auto-threaded
    /// </summary>
    /// <param name="i_Method"></param>
    public void AddAutoThreaded(Func<Task> i_Method)
    {
      if (!AreThreadsRunning)
      {
        m_Threads.Add(new Thread(() => i_Method().GetAwaiter().GetResult()));
      }
    }

    /// <summary>
    /// Adds methods to be auto-threaded
    /// </summary>
    /// <param name="i_Methods"></param>
    public void AddAutoThreaded(IEnumerable<Func<Task>> i_Methods)
    {
      if (!AreThreadsRunning)
      {
        foreach (var method in i_Methods)
        {
          AddAutoThreaded(method);
        }
      }
    }

    /// <summary>
    /// Starts all queued threads. blocks adding of new threads
    /// </summary>
    public void RunThreads()
    {
      if (!AreThreadsRunning)
      {
        AreThreadsRunning = true;
        m_Threads.ForEach(r =>
        {
          r.Start();
          r.Join();
        });
      }
    }
  }
}
