NiceUnitTesting is a simple unit testing library that supports running unit-tests in .NET Standard libraries.  
It was originally developed to run unit tests from NiceCore so it currently only has features I needed for doing that.
NiceUnitTesting is currently based on NetCore 2.0